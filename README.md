# C&H comic scrapper

#### About
C&H comic scrapper is made to scrap all the images from [C&H random comic generator](http://explosm.net/rcg) and save them in <code>./result</code> directory. <br>

#### How to use
To run the script just enter <code>npm start</code> command in console

#### Techonology used
+ [Node.js](https://nodejs.org/en/)
+ [Jimp](https://www.npmjs.com/package/jimp)
