const Jimp = require('jimp')

const results = []
const maxLength = 3
const base = 'http://files.explosm.net/rcg/'
const alphabet = 'abcdefghijklmnopqrstuwvxyz'

main()

/** Main function to run when the application is ready */
async function main () {
  await generateID()
  console.log(`+ Generated all the IDs | ${results.length}`)
  
  startDownload()
}

/** Starts an interval that downloads images based on the result array */
function startDownload () {
  setInterval(async () => { 
    if (results.length === 0) {
      console.log(`+ Job done, exiting process`)
      process.exit()
    }

    await downloadImage(results.pop()) 
  }, 10)
}

/** Generates ID using the alphabet and prefix
  
  @param {string} prefix - prefix for new ID 
*/
async function generateID (prefix = '') {
  // If the prefix has length the same as max length, go back in recursion
  if (prefix.length >= maxLength)
    return

  for (const letter of alphabet) {
    const newID = prefix + letter
    
    // If new ID length equals max length, push the result
    if (newID.length === maxLength)
      results.push(newID)
    
    // Continue the recursion
    generateID(newID)
  }
}

/** Modify image to get rid of the watermark and write it into result directory

  @param {Jimp} image - the image to save on disk
  @param {string} name - name for the image the be written as
*/
function writeImage (image, name) {
  const clone = image.clone()
  const width = clone.bitmap.width / 6

  clone.crop(clone.bitmap.width - width + 4, 0, width, 280)
  
  image.composite(clone, width, 0)
  image.crop(1, 1, width * 2 - 5, clone.bitmap.height - 2)
  
  console.log(`+ Writing ${name}`)
  image.write(`./results/_${name}.png`)
}

/** Downloads a image assigned to the provided ID and then removes the watermark
  
  @param {string} id - ID of a image to download
*/
async function downloadImage (id) {
  let error = undefined

  // Link to the image page (to get rid of the watermark i take 3 same images and connects parts into one)
  const link = base + id.repeat(3) + '.png'

  const image = await Jimp.read(link).catch((err) => error = err) 
  
  if (error !== undefined) {
    if (!error.message.includes('application/xml'))
      console.log(`- Failed to download ${id} | ${error.message}`)
    
    return
  }
  
  console.log(`+ Downloaded ${id}`)
  writeImage(image, id)
}
